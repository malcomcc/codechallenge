-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 30-04-2019 a las 01:37:36
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.1.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `codechallenge`
--
CREATE DATABASE IF NOT EXISTS `codechallenge` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `codechallenge`;

--
-- Volcado de datos para la tabla `consumptions`
--

INSERT INTO `consumptions` (`id`, `id_product`, `amount`, `voice`, `data`) VALUES
(1, 1, '10.00', '100.00', '0.00'),
(2, 1, '10.00', '87.00', '0.00'),
(3, 2, '25.00', '1219.00', '12.85'),
(4, 3, '10.00', '1.00', '0.00'),
(5, 2, '0.00', '50.00', '5.00'),
(6, 3, '10.00', '100.00', '0.00');

--
-- Volcado de datos para la tabla `products`
--

INSERT INTO `products` (`id`, `number`, `rate`, `fee`, `type`) VALUES
(1, 600123456, 'Bono 100', '5.00', 1),
(2, 600123456, 'Plan Relax', '19.00', 2),
(3, 700859665, 'Plan 10', '10.00', 4);

--
-- Volcado de datos para la tabla `services`
--

INSERT INTO `services` (`id`, `type`) VALUES
(1, 'fijo'),
(2, 'móvil'),
(3, 'fibra'),
(4, 'adsl');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
