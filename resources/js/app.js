$(document).ready( function() {    

    var app = {
        phones: [],
        consumptions: [],
        consumTable: null,
        comboID: '#phone-select',
        comboPlaceholder: 'Elija una opción',
        tableID: '#consumption_table',
        selectedPhone: 0,
        apiURL: '/api/v1/',
        init: function() {
            app.createTable();
            app.comboPlaceholder = $(app.comboID).find('option').first().removeAttr('value').attr('selected','selected');
            $(app.comboID).on('change', function() {
                app.getConsumptions( parseInt(this.value) );
            });
            app.getPhoneNumbers();
        },
        createTable: function() {
            app.consumTable = $(app.tableID).DataTable();
        },
        getPhoneNumbers: function() {
            $.ajax({
                url : app.apiURL + 'phones',
                type : 'GET'
            })
            .done( function(data) {
                app.fillCombo( data );                
            })
            .fail( function(err) {
                console.log(err);
            });
        },
        getConsumptions: function( phoneNumber ) {
            return $.ajax({
                url : app.apiURL + 'consumptions/' + phoneNumber,
                type : 'GET'
            })
            .done( function(data) {
                app.fillTable(data);                
            })
            .fail( function(err) {
                console.log(err);
            });
        },
        fillCombo: function(phonesArray) {
            $(app.comboID).html(app.comboPlaceholder);
            phonesArray.sort( function(a,b) {
                return a.number > b.number;
            });
            $(phonesArray.sort()).each( function() {
                $(app.comboID).append('<option value="' + this.number + '">' + this.number + '</option>');
            });
        },
        fillTable: function( consumptionsJSOM ) {
            app.consumTable.clear();
            $(consumptionsJSOM).each( function() {
                app.consumTable.row.add( Object.values(this) );;
            })
            .promise()
            .done( function() {
                app.consumTable.draw();
            });
        }
    }

    app.init();

    console.log('init')

});