<?php
namespace Test;

use PHPUnit\Framework\TestCase;

use App\PhoneService as PhoneService;

class GetPhonesTest extends TestCase
{
    /**
     * @runInSeparateProcess
     */
    public function testTrueAssetsToTrue()
    {
        $phoneService = new PhoneService();
        $phones = json_decode($phoneService->getPhoneNumbers());
        $assertion = true;
        foreach( $phones as $phone ) {
            if( $assertion && preg_match("/^[0-9]{3}-?[0-9]{4}-?[0-9]{4}$/", $phone->number) ) {
                $assertion = false;
            }
        }        
        $this->assertTrue($assertion, 'All items are valid phone numbers','All items are not valid phone numbers');
    }
}
?>