<?php
namespace App;

header('Content-Type: application/json');

use PDO;
use Exception;
use App\DB;

if( file_exists('../db/Db.php') ) {
    require '../db/Db.php'; // AJAX
}

class ComsumptionService {

    public function __construct()
    {
        $method = isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] !== "" ? $_SERVER['REQUEST_METHOD'] : 'get';
        switch( strtolower( $method ) ) {
            case 'get':
                $phoneNumber = isset($_GET['number']) && $_GET['number'] !== '' ? filter_var($_GET['number'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW) : '';
                http_response_code(200);
                return $this->getConsumptions($phoneNumber);                
            break;
            default:
                http_response_code(405);
        }
    }

    private function getConsumptions( int  $phoneNumber = -9999 )
    {   
        if( !is_numeric($phoneNumber) && !is_null($phoneNumber) && $phoneNumber !== 0 ) {
            throw new Exception('You have to send an integer. You have send an ' . gettype($phoneNumber) . ': ' . $phoneNumber);
        } 
        $phoneReq = $phoneNumber === -1 ? '' : ' AND products.number = ' . $phoneNumber;
        $query = 'SELECT products.fee, consumptions.voice, consumptions.data
        FROM consumptions
        JOIN products ON consumptions.id_product = products.id' . $phoneReq . '
        JOIN services ON services.id = products.type
        ORDER BY products.number';
        $db = new DB();
        $conn = $db->getConnection();
        $stmt = $conn->prepare($query);
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $stmt->execute();
        $results = $stmt->fetchAll();
        $db->close();
        echo json_encode($results);
        return json_encode($results);       
    }

}

return new ComsumptionService();
?>