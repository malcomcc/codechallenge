<?php
namespace App;

header('Content-Type: application/json');

use PDO;
use App\DB;

if( file_exists('../db/Db.php') ) {
    require '../db/Db.php'; // AJAX
}

class PhoneService {

    public function __construct() {
        $method = isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] !== "" ? $_SERVER['REQUEST_METHOD'] : 'get';
        switch( strtolower( $method ) ) {
            case 'get':
                http_response_code(200);
                return $this->getPhoneNumbers();
            break;
            default:
                http_response_code(405);
        }
    }

    public function getPhoneNumbers()
    {        
        $query = 'SELECT DISTINCT number FROM PRODUCTS';
        $db = new DB();
        $conn = $db->getConnection();
        $stmt = $conn->prepare( $query );
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $stmt->execute();
        $results = $stmt->fetchAll();       
        $db->close();
        echo json_encode($results);
        return json_encode($results);
    }

}

return new PhoneService();

?>