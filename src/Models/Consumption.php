<?php

namespace Models;

class Consumption {
    
    /**
     * Consumption Fee
     *
     * @var float
     */
    protected $fee;

    /**
     * Voice Consumption
     *
     * @var float
     */
    protected $voice;

    /**
     * Data Consumption
     *
     * @var float
     */
    protected $data;

    public function __construct( float $fee, float $voice, float $data )
    {
        $this->fee = $this->format($fee);
        $this->voice = $this->format($voice);
        $this->data = $this->format($data);
    }

    /**
     * Get Consumption fee
     *
     * @return float
     */
    public function getFee()
    {
        return $this->fee;
    }

    /**
     * Set Consumption fee
     *
     * @param float $fee
     * @return void
     */
    public function setFee( float $fee )
    {
        $this->fee = $this->format($fee);
    }

    /**
     * Get voice Consumption
     *
     * @return void
     */
    public function getVoice()
    {
        return $this->voice;
    }

    /**
     * Set voice Consumption
     *
     * @param float $voice
     * @return void
     */
    public function setVoice( float $voice )
    {
        $this->voice = $this->format($voice);
    }

    /**
     * Get data Consumption
     *
     * @return void
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set data Consumption
     *
     * @param float $data
     * @return void
     */
    public function setData( float $data )
    {
        $this->data = $this->format($data);
    }

    private function format( float $number ) {
        return number_format( $number,2);
    }

    
}

?>