<?php

namespace Models;

class Consumption {
    
    /**
     * Phone Number
     *
     * @var int
     */
    protected $number;

    /**
     * Set Phone Number
     *
     * @param int $number
     * @return int
     */
    public function setNumber( int $number )
    {
        if( is_int($number) ) {
            $this->number = $number;            
        }
        return is_int($number);        
    }

    /**
     * Get Phone Number
     *
     * @return int
     */
    public function getNumber()
    {
        return $this->number;
    }

    
}

?>