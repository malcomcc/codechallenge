<?php

namespace App;

use PDO;

if( !defined("DB_SERVER") ) {
    define("DB_SERVER","localhost");
}
if( !defined("DB_NAME") ) {
    define("DB_NAME","codechallenge");
}
if( !defined("DB_USER") ) {
    define("DB_USER","root");
}
if( !defined("DB_PASS") ) {
    define("DB_PASS","");
}

class DB {

    private $conn;

    public function __construct() {
        $this->conn = null;
    }

    public function getConnection() {
        try 
        {
            $this->conn = new PDO('mysql:host=' . DB_SERVER . ';dbname=' . DB_NAME .';charset=utf8' , DB_USER, DB_PASS,  array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
            $this->conn->exec("SET CHARACTER SET utf8");
            $this->conn->exec("set names utf8");
        }
        catch (PDOException $e) 
        {
            echo 'Error: ' . $e->getMessage();
            exit();
        }

        return $this->conn;
    }

    public function close() {
        $this->conn = null;
    }

    
}