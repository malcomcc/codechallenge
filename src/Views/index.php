<?php include 'includes/header.php';?>
    <div class="container bg-dark p-4">
      <div class="row bg-info p-4">
        <div class="col-12">
        <form>
          <select id="phone-select" class="custom-select">
            <option selected>Choose a phone number:</option>
          </select>
        </form>
        </div>
      </div>

        <div class="row">
          <div class="col-12 p-4 bg-light text-dark">
          <table id="consumption_table" class="table datatable">
            <thead>
              <tr>
                <th scope="col">Amount (€)</th>
                <th scope="col">Voice (minutos)</th>
                <th scope="col">Data (Gb)</th>
              </tr>
            </thead>
            <tbody>            
            </tbody>
          </table>
          </div>
        </div>
    </div>
    
<?php include 'includes/footer.php';?>