<?php
$code = $_SERVER['REDIRECT_STATUS'];
$codes = array(
    403 => 'You don´t have access to this page',
    404 => 'The page you request is not currently now in this site.',
    500 => 'Internal Server Error'
);
$source_url = 'http' . ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') ? 's' : '') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
if (array_key_exists($code, $codes) && is_numeric($code)) {
    $message = "Error $code: {$codes[$code]}";
} else {
    $message = 'Unknown error';
}; ?>
<?php include 'includes/header.php';?>
<div class="jumbotron">


    <h1 class="display-4">We sorry !!!</h1>
    <p class="lead">An error has ocurred.</p>
    <hr class="my-4">
    <div class="alert alert-danger">
        <p><?php echo $message;?></p>
    </div>
    <p class="lead">
        <a class="btn btn-primary btn-lsmg" href="../../public/index.php" role="button">Go to home</a>
    </p>
</div>
<?php include 'includes/footer.php';?>
<?php
die();
?>